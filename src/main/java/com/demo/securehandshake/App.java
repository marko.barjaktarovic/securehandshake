package com.demo.securehandshake;

import java.security.KeyFactory;
import java.security.NoSuchAlgorithmException;
import java.security.interfaces.RSAPrivateKey;
import java.security.interfaces.RSAPublicKey;
import java.security.spec.InvalidKeySpecException;
import java.security.spec.PKCS8EncodedKeySpec;
import java.security.spec.X509EncodedKeySpec;
import java.time.LocalDateTime;
import java.time.ZoneId;
import java.util.Base64;
import java.util.Date;

import com.auth0.jwt.JWT;
import com.auth0.jwt.algorithms.Algorithm;
import com.auth0.jwt.exceptions.JWTVerificationException;
import com.auth0.jwt.interfaces.DecodedJWT;
import com.auth0.jwt.interfaces.JWTVerifier;

public class App {

	/**
	 * Token validity period in seconds
	 */
	private static final long TOKEN_VALIDITY_PERIOD = 30L;

	private static final String ENCRYPTION_ALGORITHM = "RSA";

	private static final String ISSUER = "com.demo";

	private static final String SUBJECT = "handshake";

	public static void main(String[] args) throws NoSuchAlgorithmException, InvalidKeySpecException {

		String jwt = generateClientJWT();
		processJWTOnServer(jwt);

		System.out.println("Verified!");
	}

	private static String generateClientJWT() throws NoSuchAlgorithmException, InvalidKeySpecException {
		String privateKey = "MIIEvAIBADANBgkqhkiG9w0BAQEFAASCBKYwggSiAgEAAoIBAQCfPKKzVmN80HRs"
				+ "GAoUxK++RO3CW8GxomrtLnAD6TN5U5WlVbCRZ1WFrizfxcz+lr/Kvjtq/v7PdVOa"
				+ "8NHIAdxpP3bCFEQWku/1yPmVN4lKJvKv8yub9i2MJlVaBo5giHCtfAouo+v/XWKd"
				+ "awCR8jK28dZPFlgRxcuABcW5S5pLe4X2ASI1DDMZNTW/QWqSpMGvgHydbccI3jtd"
				+ "S7S3xjR76V/izg7FBrBYPv0n3/l3dHLS9tXcCbUW0YmIm87BGwh9UKEOlhK1NwdM"
				+ "Iyq29ZtXovXUFaSnMZdJbge/jepr4ZJg4PZBTrwxvn2hKTY4H4G04ukmh+ZsYQaC"
				+ "+bDIIj0zAgMBAAECggEAKIBGrbCSW2O1yOyQW9nvDUkA5EdsS58Q7US7bvM4iWpu"
				+ "DIBwCXur7/VuKnhn/HUhURLzj/JNozynSChqYyG+CvL+ZLy82LUE3ZIBkSdv/vFL"
				+ "Ft+VvvRtf1EcsmoqenkZl7aN7HD7DJeXBoz5tyVQKuH17WW0fsi9StGtCcUl+H6K"
				+ "zV9Gif0Kj0uLQbCg3THRvKuueBTwCTdjoP0PwaNADgSWb3hJPeLMm/yII4tIMGbO"
				+ "w+xd9wJRl+ZN9nkNtQMxszFGdKjedB6goYLQuP0WRZx+YtykaVJdM75bDUvsQar4"
				+ "9Pc21Fp7UVk/CN11DX/hX3TmTJAUtqYADliVKkTbCQKBgQDLU48tBxm3g1CdDM/P"
				+ "ZIEmpA3Y/m7e9eX7M1Uo/zDh4G/S9a4kkX6GQY2dLFdCtOS8M4hR11Io7MceBKDi"
				+ "djorTZ5zJPQ8+b9Rm+1GlaucGNwRW0cQk2ltT2ksPmJnQn2xvM9T8vE+a4A/YGzw"
				+ "mZOfpoVGykWs/tbSzU2aTaOybQKBgQDIfRf6OmirGPh59l+RSuDkZtISF/51mCV/"
				+ "S1M4DltWDwhjC2Y2T+meIsb/Mjtz4aVNz0EHB8yvn0TMGr94Uwjv4uBdpVSwz+xL"
				+ "hHL7J4rpInH+i0gxa0N+rGwsPwI8wJG95wLY+Kni5KCuXQw55uX1cqnnsahpRZFZ"
				+ "EerBXhjqHwKBgBmEjiaHipm2eEqNjhMoOPFBi59dJ0sCL2/cXGa9yEPA6Cfgv49F"
				+ "V0zAM2azZuwvSbm4+fXTgTMzrDW/PPXPArPmlOk8jQ6OBY3XdOrz48q+b/gZrYyO"
				+ "A6A9ZCSyW6U7+gxxds/BYLeFxF2v21xC2f0iZ/2faykv/oQMUh34en/tAoGACqVZ"
				+ "2JexZyR0TUWf3X80YexzyzIq+OOTWicNzDQ29WLm9xtr2gZ0SUlfd72bGpQoyvDu"
				+ "awkm/UxfwtbIxALkvpg1gcN9s8XWrkviLyPyZF7H3tRWiQlBFEDjnZXa8I7pLkRO"
				+ "Cmdp3fp17cxTEeAI5feovfzZDH39MdWZuZrdh9ECgYBTEv8S7nK8wrxIC390kroV"
				+ "52eBwzckQU2mWa0thUtaGQiU1EYPCSDcjkrLXwB72ft0dW57KyWtvrB6rt1ORgOL"
				+ "eI5hFbwdGQhCHTrAR1vG3SyFPMAm+8JB+sGOD/fvjtZKx//MFNweKFNEF0C/o6Z2FXj90PlgF8sCQut36ZfuIQ==";

		KeyFactory keyFactory = KeyFactory.getInstance(ENCRYPTION_ALGORITHM);
		PKCS8EncodedKeySpec keySpecPKCS8 = new PKCS8EncodedKeySpec(Base64.getDecoder().decode(privateKey));
		RSAPrivateKey rsaPrivateKey = (RSAPrivateKey) keyFactory.generatePrivate(keySpecPKCS8);

		Algorithm signingAlgorithm = Algorithm.RSA256(null, rsaPrivateKey);
		LocalDateTime expiresAt = LocalDateTime.now().plusSeconds(TOKEN_VALIDITY_PERIOD);

		return JWT.create()
				.withExpiresAt(
						Date.from(expiresAt
								.atZone(ZoneId.systemDefault())
								.toInstant()))
				.withIssuer(ISSUER)
				.withSubject(SUBJECT)
				.withAudience("YOUR_PERMANENT_CLIENT_ID")
				.sign(signingAlgorithm);
	}

	/**
	 * Verifies token and processes it further, based on custom business logic.
	 * 
	 * @param jwt represents client generated JWT
	 * @throws NoSuchAlgorithmException
	 * @throws InvalidKeySpecException
	 * @throws JWTVerificationException if token validation fails
	 */
	private static void processJWTOnServer(String jwt) throws NoSuchAlgorithmException, InvalidKeySpecException {
		String publicKey = "MIIBIjANBgkqhkiG9w0BAQEFAAOCAQ8AMIIBCgKCAQEAnzyis1ZjfNB0bBgKFMSv"
				+ "vkTtwlvBsaJq7S5wA+kzeVOVpVWwkWdVha4s38XM/pa/yr47av7+z3VTmvDRyAHc"
				+ "aT92whREFpLv9cj5lTeJSibyr/Mrm/YtjCZVWgaOYIhwrXwKLqPr/11inWsAkfIy"
				+ "tvHWTxZYEcXLgAXFuUuaS3uF9gEiNQwzGTU1v0FqkqTBr4B8nW3HCN47XUu0t8Y0"
				+ "e+lf4s4OxQawWD79J9/5d3Ry0vbV3Am1FtGJiJvOwRsIfVChDpYStTcHTCMqtvWb"
				+ "V6L11BWkpzGXSW4Hv43qa+GSYOD2QU68Mb59oSk2OB+BtOLpJofmbGEGgvmwyCI9MwIDAQAB";

		X509EncodedKeySpec spec = new X509EncodedKeySpec(Base64.getDecoder().decode(publicKey));
		KeyFactory kf = KeyFactory.getInstance(ENCRYPTION_ALGORITHM);
		RSAPublicKey rsaPublicKey = (RSAPublicKey) kf.generatePublic(spec);
		Algorithm verificationAlgorithm = Algorithm.RSA256(rsaPublicKey, null);

		DecodedJWT decodedJwt = JWT.decode(jwt);
		JWTVerifier verifier = JWT.require(verificationAlgorithm)
				.withIssuer(ISSUER)
				.withSubject(SUBJECT)
				.withAudience(decodedJwt.getAudience().get(0))
				.build();

		verifier.verify(jwt);

		// TODO - add some business logic here
	}
}
